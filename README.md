Although I'm confident in my ability to quickly pick up Ruby, I ended up only having a couple of evenings to work on this project, so I decided to just have a little fun prototyping a new feature for Monkeybadger in Python.

I started by making create_logfile.py, which, as the name suggests, creates a dummy logfile. The entries in this tab-delimited file contain an id number, a datetime stamp, a priority level, and an error message, in that order. The ids are sequentially numbered and the datetime stamps are randomized on a uniform distribution over a 24 hour period. The priority levels of 'IGNORE', 'LOW PRIORITY', and 'HIGH PRIORITY' are given weights so as to respectively occur 10, 7, and 4 times more frequently than that of 'CRITICAL'. I generated the error message by implementing a Chomskybot-esque module, chomsky.py. These messages thus have the property of being generated from a finite and well-defined vocabulary. During the initial iteration of log entry creation, no attempt to assign any aspect or feature of a Chomskybot string to a given priority level was made.

This first pass at a logfile can be tested:

Mímisbrunnr:Chloe+Isabel Brandon$ head -5 dummy.log 
1	2013-08-28 00:00:28	HIGH PRIORITY	If the position of the trace in (99c) were only relatively inaccessible
to movement, the earlier discussion of deviance is unspecified with
respect to a descriptive fact.
2	2013-08-28 00:04:41	HIGH PRIORITY	Clearly, the appearance of parasitic gaps in domains relatively
inaccessible to ordinary extraction appears to correlate rather closely

So far, so good: we have a reasonable simulacrum of a log file that returns semi-standardized error messages. But what to do with it? One question that comes to mind involves the error messages that are labeled 'CRITICAL'. Suppose that we think we have a good idea of what error messages are associated with a label class, e.g., 'CRITICAL', and we tentatively trust the Bayesian filter to return true CRITICAL logs. All the same, we might like to know when a log with a 'CRITICAL' label and a new type of error message shows up, so that we can train the filter with it. Of course, rather than manually scanning the log files, we would prefer to have some measure of automation involved in this.  

In other words, we want a measure of how surprised we are at a result, and this leads us to think of entropy. In particular, we are really thinking of a sort of relative entropy: given the distribution of terms derived from messages in a set of logs with a 'CRITICAL' label, how similiar is the distribution of terms from another 'CRITICAL' log? In other words, we want the Kullback-Leibler divergence.

get_info.py reads in the dummy.log file and collects the 'CRITICAL' messages. It removes from this set one entry whose message will act as a control: given that this entry has text that is derived from the same vocabulary used by the messages in the larger set, what kind of divergence in entropy can we expect between this text and the larger text collection? It also removes another test entry created earlier that is in line with all other entries save in one aspect: its message is not Chomsky-esque. There will be a divergence between the text of this message and the collected text of the larger set as well. One assumes that this divergence will be larger, indicating a greater surprise than normal.

It turns out that this does seem to be the case -- manually running get_info.py five times yielded the following:

>>> 
D(P||Qv1) = 22.350 bits
D(P||Qv2) = 29.452 bits
>>> ================================ RESTART ================================
>>> 
D(P||Qv1) = 27.275 bits
D(P||Qv2) = 29.235 bits
>>> ================================ RESTART ================================
>>> 
D(P||Qv1) = 20.815 bits
D(P||Qv2) = 29.254 bits
>>> ================================ RESTART ================================
>>> 
D(P||Qv1) = 24.652 bits
D(P||Qv2) = 29.309 bits
>>> ================================ RESTART ================================
>>> 
D(P||Qv1) = 22.595 bits
D(P||Qv2) = 29.256 bits

YMMV, but we do see that the test entry consistently displays a larger entropy relative to the sample set than does the control. 

Of course, in order to even consider using this sort of thing in production, one would need a much larger sample set, a better idea of what a proper control entropy is, a notion of what sufficiently large disparity between control and test should trigger internal notifications, etc. But it's an interesting exercise nonetheless that points to a relatively simple way of having more data about your logs.
 
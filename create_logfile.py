
# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
create_logfile module

Makes a dummy logfile for further analyses.
  
@author: Brandon Devine
@contact: brandon.devine@gmail.com
'''

import random, datetime, chomsky

def get_ids(num=10):
    ids = []
    for _ in range(1,num+1):
        ids.append(_)
    return ids
    
def make_random_timestamp(beginning, end):
    delta = end - beginning
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return beginning + datetime.timedelta(seconds=random_second)

def get_sorted_timestamps(d1,d2,num=10):
    stamps = []
    for _ in range(num):
        stamps.append(make_random_timestamp(d1,d2))
    stamps.sort()
    return stamps

def get_log_levels(level_list,num=10):
    log_levels = []
    for _ in range(num):
        log_levels.append(random.choice([v for v,c in level_list for _ in range(c)]))
    return log_levels

def get_log_messages(num=10):
    messages = []
    for _ in range(num):
        messages.append(chomsky.create_chomsky_message())
    return messages
    

def main(filename,lognum=10):
    ids = get_ids(num=lognum)
    
    d1 = datetime.datetime.strptime('8/28/2013 12:00 AM', '%m/%d/%Y %I:%M %p')
    d2 = datetime.datetime.strptime('8/28/2013 11:59 PM', '%m/%d/%Y %I:%M %p')
    timestamps = get_sorted_timestamps(d1,d2,num=lognum)
    
    weighted_levels = [('CRITICAL',1),('HIGH PRIORITY',4),('LOW PRIORITY',7),('IGNORE',10)]
    log_levels = get_log_levels(weighted_levels,num=lognum)

    log_messages = get_log_messages(num=lognum)

    logs = zip(ids,timestamps,log_levels,log_messages)

    criticals = []
    for log in logs:
        if log[2] == 'CRITICAL':
            criticals.append(log)
    if len(criticals) == 0:
        print 'No CRITICAL statuses available to modify.'
        return
    else:
        choice = random.choice(criticals)
        test = choice[0],choice[1],choice[2],'We are what we do repeatedly. Excellence, then, is not an act, but a habit.'
        for log in logs:
            if log[0] == test[0]:
                logs.remove(log)
                logs.insert(test[0]-1,test)

    with open(filename,"w+") as f:
        for log in logs:
            f.write('\t'.join(map(str,log)))
            f.write('\n')
    print 'Log file successfully created.'
    

if __name__ == '__main__':
    main("dummy.log",lognum=500)

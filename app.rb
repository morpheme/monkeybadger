module MonkeyBadger

  class App < Sinatra::Application

    before do
      @unclassified_count = Data.redis.scard("unclassified")
      @classified_count = Data.redis.scard("classified")
      @label_classes = {
        'Ignore' => 'success',
        'Low Priority' => 'info',
        'High Priority' => 'warning',
        'Critical' => 'danger'
      }
    end

    get '/' do
      erb :index   
    end

    get '/inspect' do
      @total_words = Data.classifier.instance_variable_get(:@total_words)
      @categories = Data.classifier.instance_variable_get(:@categories)
      @max_counts = @categories.inject({}){|o,kv| o[kv[0]] = kv[1].values.max; o}
      erb :inspect
    end

    get '/reset' do
      Data.reset_classifier
      Data.reset_training
      erb :reset
    end

    get '/unclassified' do
      @unclassified = Data.redis.smembers("unclassified")[0..20]
      erb :unclassified
    end

    get '/classified' do
      @classified = Data.redis.smembers("classified")[0..20]
      @classifications = @classified.map{|sha| Data.redis.get "#{sha}_classification"}
      erb :classified
    end

    get '/train' do
      Data.train
      @count = Data.redis.scard("classified")
      erb :train
    end

    get '/classify/:sha/:classification' do |sha, classification|
      Data.classify(sha, classification)
    end

    get '/blob/:sha' do |sha|
      Data.get(sha)
    end

    get '/create' do
      Data.create(params[:exception], params[:backtrace], params[:environment])
    end

  end

end

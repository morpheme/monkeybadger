# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
utilities module

Contains the heavy-lifting functions for text processing and infometrics.
  
@author: Brandon Devine
@contact: brandon.devine@gmail.com
'''

import string, re
from math import log

def clean_text(text):
    """
    Gives a cursory once-over for text normalization of entries in
    an error log.
    """
    text = text.split('\t')
    text[3] = re.sub(r'\.','',text[3])
    text[3] = re.sub(r',','',text[3])
    text[3] = text[3].rstrip('\n')
    text[3] = text[3].lower()
    return text

def make_norm_freq_dist(iterable):
    """Compute the empirical (normalized) distribution of a sequence."""
    sigma = {}
    n = 0.0
    for elem in iterable:
            sigma[elem] = sigma.get(elem,0.0) + 1.0
            n += 1.0
    return dict((k,v/n) for (k,v) in sigma.iteritems())

def get_KLdiv(p,q):
    """
    Calculates the Kullback-Leibler divergence, a.k.a. relative entropy,
    a.k.a. information gain.

    The KLdiv is the asymmetric divergence of the approximate distribution
    Q from the gold standard distribution P; i.e., the average amount of
    information wasted in encoding events from P with a code based on Q.
    It can informally be thought of as the 'distance' between two
    distributions, but that glosses over its asymmetry.
    """
    return sum(p[x]*(log(p[x],2)-log(q.get(x,1e-12),2)) for x in p.iterkeys())


txt1 = 'Goldman Sachs and Morgan Stanley, the last big independent investment banks on Wall Street, will \
transform themselves into bank holding companies subject to far greater regulation, the Federal Reserve said \
Sunday night, a move that fundamentally reshapes an era of high finance that defined the modern Gilded Age.'

txt2 = 'The firms requested the change themselves, even as Congress and the Bush administration rushed to pass \
a $700 billion rescue of financial firms. It was a blunt acknowledgment that their model of finance and \
investing had become too risky and that they needed the cushion of bank deposits that had kept big commercial \
banks like Bank of America and JPMorgan Chase relatively safe amid the recent turmoil.'

def main():     #pretty much whatever I tested last
    p = make_norm_freq_dist(skip_whitespace(clean_text(txt1)))
    q = make_norm_freq_dist(skip_whitespace(clean_text(txt2)))
    print 'For distributions over letters in the input texts:'
    print 'H(P) = %.3f bits' % get_entropy(p)
    print 'D(P||Q) = %.3f bits' % get_KLdiv(p,q)
    
if __name__ == '__main__':
    main()

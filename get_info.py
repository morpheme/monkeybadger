# -*- coding: utf-8 -*-
#!/usr/bin/env python
'''
get_info module

Gets data from logfile and assesses Kullback-Leibler divergence. 
  
@author: Brandon Devine
@contact: brandon.devine@gmail.com
'''

from utilities import clean_text, make_norm_freq_dist, get_KLdiv

import string, random

with open('dummy.log','r') as f:
    criticals = []
    for log in f.readlines():
        log = clean_text(log)
        if log[2] == 'CRITICAL':
            criticals.append(log)
            
# remove the outlier CRITICAL that will be our test message. We could have created it here in
# the first place but then would have had to allow some sloppiness in distribution of other
# features such as datetime stamp.
for log in criticals:
    if log[3] == 'we are what we do repeatedly excellence then is not an act but a habit':
        test = log[3].split()
        criticals.remove(log)
        
# remove the held-out CRITICAL that we'll use as a control.
choice = random.choice(criticals)
held_out = choice
held_out = held_out[3].split()
criticals.remove(choice)

# finally winnow down the rest of the logs to just the error messages
messages = []
for log in criticals:
    messages.extend(log[3].split())

# let's crunch some numbers
p = make_norm_freq_dist(messages)
qC = make_norm_freq_dist(held_out)
qT = make_norm_freq_dist(test)
print 'D(P||Qv1) = %.3f bits' % get_KLdiv(p,qC)
print 'D(P||Qv2) = %.3f bits' % get_KLdiv(p,qT)
                                                    
